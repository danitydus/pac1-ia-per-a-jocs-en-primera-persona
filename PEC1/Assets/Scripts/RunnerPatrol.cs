using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.SceneManagement;
public class RunnerPatrol : MonoBehaviour
{
    // La �nica variable necesaria para el runner: el fantasma que tendr� que seguir en todo momento.
    public GameObject ghost;
    // Al iniciar, el agente runner copiar� la velocidad del fantasma que sigue pero con un valor siempre
    // menor, para que el fantasma siempre este por delante.
    void Start()
    {
        NavMeshAgent agente = GetComponent<NavMeshAgent>();
        agente.speed = (ghost.GetComponent<NavMeshAgent>().speed * 0.8f);
    }
    // Durante todo momento la destinaci�n del agente runner ser� la misma que la del fantasma que sigue.
    void Update()
    {
        NavMeshAgent agente = GetComponent<NavMeshAgent>();
        agente.destination = ghost.transform.position;

        if (Input.GetKeyUp(KeyCode.Space))
        {
            SceneManager.LoadScene(0);
        }
    }
}
